<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Category;
use App\Models\Seller;
use App\Models\Transaction;



class Product extends Model
{
    use HasFactory;

    const PRODUCTO_NO_DISPONIBLE = 'no disponible';
    const PRODUCTO_DISPONIBLE = 'disponible';

    protected $filelable = [
        'name',
        'description',
        'status',
        'quantity',
        'image',
        'seller_id',
    ];

    public function estaDisponible()
    {
        return $this->status == Product::PRODUCTO_DISPONIBLE;
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function seller()
    {
        return $this->belongsTo(Seller::class);
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }
    

}
